package Homework03.arraysort;

import java.util.Arrays;

public class arraysort {

    public static void main (String[] args) {
        int[] array = new int[]{34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20};
        System.out.println(Arrays.toString(array));
        Arraysort(array, 1, 3);
        Arraysort(array, 2, 4);
        Arraysort(array, 5, 10);
        Arraysort(array, 3, 6);
        Arraysort(array, 4, 9);
        System.out.println(Arrays.toString(array));

    }
    public static void Arraysort (int[] array, int index1, int index2) {
        int temp = array[index1];
        array[index1] = array[index2];
        array[index2] = temp;
    }
}
