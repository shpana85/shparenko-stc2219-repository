package Homework03.arrays;

public class Searchingindex {

    public static void main(String[] args) {
        int[] array = new int[10];

        for (int i = 0; i < array.length; i++) {
            array[i] = i;
        }

        System.out.println(Searchingindexarray(array, 5));
    }
    public static int Searchingindexarray (int[] array, int number) {

        for (int i = 0; i < array.length; i++) {
            if (array[i] == number) {
                return i;
            }
        }
        return -1;
    }
}

